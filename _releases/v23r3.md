---
version: v23r3
date: 2012-06-20
supported_builds: [CMT]
---
## Release Notes
<h3>Externals version: <a href="http://lcginfo.cern.ch/release/64/>LCGCMT_64</a></h3>
<h3>General Changes</h3>
<ul>
<li>Fixed compilation warnings and errors on various platforms (Clang 3.0, gcc 4.7, gcc with c++11 enabled)<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Added configuration files to build Gaudi with CMake (all the versions numbers of the packages had to be increased).<br/>
    They are in an alpha state, so not ready for production and the API is not stable, but the builds are equivalent to the CMT ones.<br/>
    To test, run the script <code>configure</code> in the top level directory and follow the instructions.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
<h3>Packages Changes</h3>
<ul>
<li>GaudiPolicy (v12r1):
<ul>
<li>Use the compiler native dependencies from CMT when using CMT v1r25.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Added fragment to install generic resources (install_resources).<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Modified GaudiTest.GaudiExeTest to be able to run executables with or without
   the <code>.exe</code> extension.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiAlg (v14r2):
<ul>
<li><a href="https://savannah.cern.ch/patch/index.php?5410">Patch #5410</a>: Enable the suppression of misbalance summary from GaudiTool<br/>
    (<span class="author">Vanya Belyaev</span>)</li>
<li><a href="https://savannah.cern.ch/task/index.php?22742">Task #22742</a>: Optimize the GaudiAlgorithm get() functions<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li><a href="https://savannah.cern.ch/patch/index.php?5379">Patch #5379</a>: Improvements for GaudiAlgs.py<br/>
    (<span class="author">Vanya Belyaev</span>)</li>
</ul>
</li>
<li>GaudiProfiling (v1r5):
<ul>
<li>Disable on MacOSX (cannot compile).<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiCommonSvc (v1r2):
<ul>
<li><a href="https://savannah.cern.ch/patch/index.php?5390">Patch #5390</a>: fix unprotected VERBOSE and DEBUG messages<br/>
    (<span class="author">Marco Cattaneo</span>)</li>
</ul>
</li>
<li>GaudiPython (v12r2):
<ul>
<li><a href="https://savannah.cern.ch/patch/index.php?5379">Patch #5379</a>: Improvements for GaudiAlgs.py<br/>
    (<span class="author">Vanya Belyaev</span>)</li>
</ul>
</li>
<li>GaudiExamples (v23r3):
<ul>
<li><a href="https://savannah.cern.ch/task/index.php?22742">Task #22742</a>: Optimize the GaudiAlgorithm get() functions<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li><a href="https://savannah.cern.ch/patch/index.php?5379">Patch #5379</a>: Improvements for GaudiAlgs.py<br/>
    (<span class="author">Vanya Belyaev</span>)</li>
</ul>
</li>
</ul>
<!-- ====================================================================== -->
