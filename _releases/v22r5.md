---
version: v22r5
date: 2011-11-11
supported_builds: [CMT]
without_docs: true
---
## Release Notes
<h3>Externals version: <a href="http://lcginfo.cern.ch/release/61b/>LCGCMT_61b</a></h3>
<h3>General Changes</h3>
<ul>
<li>Minor changes to support C++0x/11.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
<h3>Packages Changes</h3>
<ul>
<li>GaudiPolicy (v11r5):
<ul>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?86424">bug #86424</a>: Problem with <a href="https://savannah.cern.ch/patch/index.php?5025">patch #5025</a><br/>
    Remove the added option for ICC (produces a warning).<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>added the -we1011 and -we117 for the icc compiler. This is to mimic the
    behavior of gcc's -Werror=return-type<br/>
    (<span class="author">Hubert Degaudenzi</span>)</li>
</ul>
</li>
<li>GaudiKernel (v27r17):
<ul>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?87340">bug #87340</a>: code bloat because of MessageStream operator&lt;&lt;<br/>
   Added (as suggested) an explicit implementation of operator&lt;&lt; for
   "const char *"<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?87341">bug #87341</a>: performance problem in ChronoEntity<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiSvc (v18r17):
<ul>
<li>THistSvc: call Reset() on all TTrees and THNs in io_reinit when copying to
   worker directory.<br/>
    (<span class="author">Charles Leggett</span>)</li>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?87253">bug #87253</a>: Don't override exception type when re-throwing<br/>
    (<span class="author">Charles Leggett</span>)</li>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?86086">bug #86086</a>: print warning message when an AlgTool fails to finalize<br/>
    (<span class="author">Charles Leggett</span>)</li>
</ul>
</li>
<li>GaudiProfiling (v1r2):
<ul>
<li>added cstddef include for the size_t type definition which apparently has been
   moved in Gcc 4.6<br/>
    (<span class="author">Hubert Degaudenzi</span>)</li>
</ul>
</li>
<li>Gaudi (v22r5):
<ul>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?86980">bug #86980</a>: gaudirun.py should prevent the import of GaudiPython<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Introduced a hook to override the default gaudirun main loop<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
</ul>
<!-- ====================================================================== -->
