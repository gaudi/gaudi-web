---
version: v23r0
date: 2012-01-25
supported_builds: [CMT]
---
## Release Notes
<h3>Externals version: <a href="http://lcginfo.cern.ch/release/62a/>LCGCMT_62a</a></h3>
<h3>General Changes</h3>
This is a major release of Gaudi. The main changes are:
<ul>
<li>Repackaging of GaudiSvc mainly to reduce the size of the libraries that users
    may need to override (<a href="https://savannah.cern.ch/task/index.php?23931">task #23931</a>, <a href="https://savannah.cern.ch/patch/index.php?1816">patch #1816</a>).<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Removed GaudiPoolDb and introduced RootCnv.<br/>
    The Application Area project POOL is not used anymore, but an alternative
    based on ROOT (can read POOL files) has been copied from LHCb. The new
    conversion service is still developed in LHCb and comments/bug reports have
    to be directed to Markus Frank.<br/>
    (<span class="author">Marco Clemencic</span>, <span class="author">Markus Frank</span>)</li>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?85377">bug #85377</a>: GaudiTuple tuple does not understand ulonglong<br/>
    (<span class="author">Chris Jones</span>)</li>
<li>Modified ApplicationMgr to return non-zero in case of failure during finalization
    and to print different messages in finalization and termination in case of errors.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
<h3>Packages Changes</h3>
<ul>
<li>GaudiPolicy (v12r0):
<ul>
<li><a href="https://savannah.cern.ch/patch/index.php?5165">Patch #5165</a>: Change GaudiPolicy requirements to allow compilation with -O3
    (CMT tag "use-O3")<br/>
    (<span class="author">Ben Couturier</span>)</li>
<li>Moved the genConf directories of the packages inside the binary directory.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Added the <i>fake</i> CMTCONFIGs *-test (only few of them), implying the tag
    target-test, which should be used for tunable (by experiments) tests.<br/>
    <i><font color="red">Not yet functional</font></i><br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Added the pattern PackageVersionHeader, similar to ProjectVersionHeader, but
    dedicated to packages and with the possibility of specifying the destination
    directory of the generated header.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiKernel (v28r0):
<ul>
<li>Added interfaces for tools used to add dynamic mapping to DataOnDemandSvc:
    IDODNodeMapper, IDODAlgMapper.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?89628">bug #89628</a>: Missing const methods in CommonMessaging.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?89653">bug #89653</a>: Gaudi is not ready for Boost 1.48 (filesystem V3)<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Removed the unused FastContainerSvc.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiCoreSvc (v1r0):
<ul>
<li>Modified ServiceManager to avoid race conditions on concurrent requests of
   services from different threads.<br/>
    (<span class="author">Illya Shapoval</span>)</li>
<li><a href="https://savannah.cern.ch/patch/index.php?5183">Patch #5183</a>: JobOptionsSvc: warnings and environment variables<br/>
    (<span class="author">Sasha Mazurov</span>)</li>
<li>Modified DataOnDemandSvc to accept tools to map paths to node types
    (IDODNodeMapper) and to algorithms (IDODAlgMapper).<br/>
    Added a basic IDODNodeMapper and IDODAlgMapper (DODBasicMapper) implementing
     the minimal DataOnDemandSvc static mapping.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li><a href="https://savannah.cern.ch/patch/index.php?5176">Patch #5176</a>: fix problem with underscore in property names (Boost 1.48)<br/>
    (<span class="author">Sasha Mazurov</span>)</li>
<li>Make ApplicationMgr print the version of the package if no version is
    specified in the options.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li><a href="https://savannah.cern.ch/patch/index.php?5166">Patch #5166</a>: Allow "::" in property and component names.<br/>
    (<span class="author">Sasha Mazurov</span>)</li>
</ul>
</li>
<li>GaudiMonitor (v3r0):
<ul>
<li>Removed DataListenerSvc.<br/>
    Moved from GaudiSvc: ExceptionSvc, HistorySvc, IssueLogger.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiPartProp (v1r0):
<ul>
<li>New package: contains Gaudi::ParticlePropertySvc, originally in GaudiSvc. (<a href="https://savannah.cern.ch/task/index.php?23931">task #23931</a>, <a href="https://savannah.cern.ch/patch/index.php?1816">patch #1816</a>)<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiSvc (v19r0):
<ul>
<li>Moved several services from GaudiSvc to other packages. (<a href="https://savannah.cern.ch/task/index.php?23931">task #23931</a>, <a href="https://savannah.cern.ch/patch/index.php?1816">patch #1816</a>)<br/>
    See package release notes for details.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?89653">bug #89653</a>: Gaudi is not ready for Boost 1.48 (filesystem V3)<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiUtils (v4r0):
<ul>
<li>Imported HistoStrings functions from (LHCb) Kernel/HistoStrings.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>RootCnv (v1r12):
<ul>
<li>Imported from LHCb (Online/RootCnv), with minor changes.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiCommonSvc (v1r0):
<ul>
<li>New package: contains services used by most applications. (<a href="https://savannah.cern.ch/task/index.php?23931">task #23931</a>, <a href="https://savannah.cern.ch/patch/index.php?1816">patch #1816</a>)<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiPython (v12r0):
<ul>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?88448">bug #88448</a>: HistoFile does not know TFile<br/>
    (<span class="author">Marco Clemencic</span>)</li>
<li>First prototype of generic/extensible persistency configuration.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiMP (v1r3):
<ul>
<li>Address failures due to missing temporary files.<br/>
    (<span class="author">Wim Lavrijsen</span>)</li>
</ul>
</li>
<li>Gaudi (v23r0):
<ul>
<li>Added support for class aliases in the meta-module "Configurables".<br/>
    Added RootCnvSvc and RootEvtSelector to the list of aliased services
    (enforcing the default name to preserve compatibility with LHCb).<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiExamples (v23r0):
<ul>
<li>Major restructuring of tests:
    <ul>
    <li>replaced POOL persistency with the new ROOT one</li>
    <li>removed obsolete test runner scripts</li>
    <li>added tests/qmtest/refs</li>
    <li>added tests/data</li>
    </ul>
    (<span class="author">Marco Clemencic</span>)</li>
<li>Added test for dynamic configuration of DataOnDemandSvc.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
</ul>
<hr/>
<a href="release.notes.v22.html">Older versions</a>
</body>
</html>
