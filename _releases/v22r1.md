---
version: v22r1
date: 2011-02-24
supported_builds: [CMT]
---
## Release Notes
<h3>Externals version: LCGCMT_60a</h3>
<h3>General Changes</h3>
<ul>
<li>Updated to pick up the patch release of ROOT (with a fixed memory leak)</li>
</ul>
<h3>Packages Changes</h3>
<ul>
<li>GaudiPolicy (v11r1):
<ul>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?77806">bug #77806</a>: createProjVersHeader breaks with Atlas style tags
    allow Atlas style GaudiPKJ-XX-YY-ZZ tags<br/>
    (<span class="author">Charles Leggett</span>)</li>
</ul>
</li>
<li>GaudiKernel (v27r13):
<ul>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?77534">bug #77534</a>: this-&gt; needed in Property.h<br/>
    (<span class="author">Charles Leggett</span>)</li>
</ul>
</li>
<li>GaudiSvc (v18r13):
<ul>
<li>Added another special case for the order of service finalization
    (NTupleSvc after ToolSvc, because of GaudiTupleTool)<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiPoolDb (v5r11):
<ul>
<li><a href="https://savannah.cern.ch/patch/index.php?4662">Patch #4662</a>: Fix for ROOT <a href="https://savannah.cern.ch/bugs/index.php?77747">bug #77747</a><br/>
    (<span class="author">Markus Frank, Marco Clemencic</span>)</li>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?78456">bug #78456</a>: warning/error when building GaudiPoolDb<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiAlg (v13r6):
<ul>
<li>Fixes for ICC<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
</li>
<li>GaudiGSL (v7r9):
<ul>
<li>Fixed <a href="https://savannah.cern.ch/bugs/index.php?77948">bug #77948</a>: GaudiGSL/requirements calls for lcg_dictionary<br/>
    (<span class="author">Charles Leggett, Marco Clemencic</span>)</li>
</ul>
</li>
</ul>
<!-- ====================================================================== -->
