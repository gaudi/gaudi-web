---
version: v25r6p1
date: 2014-12-09
supported_builds: [CMake, CMT]
---
## Release Notes
<h3>Externals version: <a href="http://lcginfo.cern.ch/release/71root6/>LCGCMT_71root6</a></h3>
<h3>General Changes</h3>
<ul>
<li>Changed the version LCG to 71root6.<br/>
    (<span class="author">Marco Clemencic</span>)</li>
</ul>
<h3>Packages Changes</h3>
<ul>
</ul>
<!-- ====================================================================== -->
