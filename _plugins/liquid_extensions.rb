module Jekyll
  module ExtraFilters
    # Sort elements of an array by numeric values
    def sort_version(input, property = nil)
      ary = input.flatten

      if property.nil?
        ary.sort_by { |v| v.scan(/\d+|\D+/).map {|i| if i =~ /[[:digit:]]/ then i.to_i else i end} }
      elsif ary.empty? # The next two cases assume a non-empty array.
        []
      elsif ary.first.respond_to?(:[]) && !ary.first[property].nil?
        ary.sort_by { |v| v[property].scan(/\d+|\D+/).map {|i| if i =~ /[[:digit:]]/ then i.to_i else i end} }
      end
    end

    # Format doxygen URL for a given project version
    def doxygen_url(input)
      conf = @context.registers[:site].config
      base = ""
      if Jekyll.env == "development"
        base = conf["url"].chomp("/")
      end
      "#{base}#{conf["baseurl"]}/doxygen/#{input}/index.html"
    end
  end
end

Liquid::Template.register_filter(Jekyll::ExtraFilters)
