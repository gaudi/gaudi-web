#!/usr/bin/env python

import os
from os.path import dirname, realpath, join, exists

import re

from collections import OrderedDict
from urllib2 import urlopen
from subprocess import call, check_output
from shutil import rmtree
from itertools import dropwhile, takewhile, islice, ifilter

def write_front_matter(fileobject, data, **kwargs):
    fileobject.write('---\n')
    fileobject.writelines('{0}: {1}\n'.format(*item) for item in data.items())
    fileobject.writelines('{0}: {1}\n'.format(*item) for item in kwargs.items())
    fileobject.write('---\n')

def skip_front_matter(iterable):
    class count_markers(object):
        def __init__(self):
            self.count = 2
        def __call__(self, l):
            if self.count <= 0:
                return False
            elif l.strip() == '---':
                self.count -= 1
            return True
    return dropwhile(count_markers(), iterable)


def add_issue_links(iterable):
    savannah = re.compile(r'\b([Pp]atch|[Tt]ask|[Bb]ug) #([0-9]+)')
    def sav_repl(m):
        return ('<a href="https://savannah.cern.ch/{1}/index.php?{2}">{0}</a>'
                .format(m.group(0),
                        'bugs' if m.group(1).lower() == 'bug'
                        else m.group(1).lower(),
                        m.group(2)))
    JIRA = re.compile(r'\b([A-Z]+)-[0-9]+')
    def JIRA_repl(m):
        sft = ['ROOT', 'SPI', 'PF', 'CFHEP', 'CVM']
        return ('<a href="https://{1}its.cern.ch/jira/browse/{0}">{0}</a>'
                .format(m.group(0), 'sft.' if m.group(1) in sft else ''))
    for l in iterable:
        yield JIRA.sub(JIRA_repl, savannah.sub(sav_repl, l))


def main():
    base = dirname(dirname(realpath(__file__)))

    # list declared releases
    versions = dict((v.split('.')[0], join(base, '_releases', v))
                    for v in os.listdir(join(base, '_releases'))
                    if not v.startswith('.'))

    # get available Doxygen docs
    docs = set(urlopen('http://cern.ch/gaudi/doxygen/list.php').read()
               .splitlines())

    tmpdir = join(base, 'tmp-Gaudi')

    req_file = join(tmpdir, 'GaudiRelease', 'cmt', 'requirements')

    call(['git', 'clone', '-n', 'https://gitlab.cern.ch/gaudi/Gaudi.git',
          tmpdir])
    call(['git', 'fetch', '--tags'], cwd=tmpdir)
    for v in versions:
        print 'Checking', v
        with open(versions[v]) as rel:
            meta = [l.strip().split(':', 1) for l in
                    takewhile(lambda x: x.strip() != '---',
                              dropwhile(lambda x: x.strip() == '---', rel))]
            meta = OrderedDict((k, v.strip()) for k, v in meta)
            orig = OrderedDict(meta)

        without_docs = 'true' if v not in docs else 'false'
        if meta.get('without_docs', 'false') != without_docs:
            meta['without_docs'] = without_docs

        rel_notes_ext = None
        if check_output(['git', 'tag', '-l', v], cwd=tmpdir):
            print 'tag found, extracting info'
            if not os.path.isdir(join(base, 'releases', v)):
                os.makedirs(join(base, 'releases', v))

            if os.path.isdir(join(tmpdir, 'GaudiRelease')):
                rmtree(join(tmpdir, 'GaudiRelease'))
            call(['git', 'checkout', '-f', v, '--', 'GaudiRelease'], cwd=tmpdir)
            if exists(req_file):
                with open(join(base, 'releases', v, 'requirements.html'), 'w') as req:
                    write_front_matter(req, meta, layout='requirements')
                    req.writelines(open(req_file))
                with_requirements = True
            else:
                with_requirements = False

            with_cmake = exists(join(tmpdir, 'GaudiRelease', 'CMakeLists.txt'))

            supported_builds = [b for b, f in zip(['CMake', 'CMT'],
                                                  [with_cmake,
                                                   with_requirements])
                                if f]
            meta['supported_builds'] = '[{0}]'.format(', '.join(supported_builds))

            for f in os.listdir(join(tmpdir, 'GaudiRelease', 'doc')):
                if re.match(r'release\.notes.*\.html', f):
                    with open(join(base, 'releases', v, f), 'w') as rel:
                        write_front_matter(rel, meta, layout='release_notes')
                        rel.writelines(open(join(tmpdir, 'GaudiRelease', 'doc', f)))

            if exists(join(tmpdir, 'GaudiRelease', 'doc', 'release.notes.html')):
                rel_notes_ext = list(add_issue_links(
                    takewhile(
                        lambda l: not l.strip().startswith('<h2>'),
                        islice(dropwhile(
                            lambda l: not re.match(r'^\s*<h2>.*' + v, l),
                            open(join(tmpdir, 'GaudiRelease', 'doc', 'release.notes.html'))
                        ), 1, None)
                    )
                ))
        else:
            print 'tag missing!'
            meta['tag_missing'] = 'true'

        if rel_notes_ext or meta != orig:
            # update metadata
            with open(versions[v]) as rel:
                old = rel.readlines()
            with open(versions[v], 'w') as rel:
                write_front_matter(rel, meta)
                rel.writelines(skip_front_matter(old))
                if rel_notes_ext and '## Release Notes' not in set(l.strip() for l in old):
                    rel.write('## Release Notes\n')
                    rel.writelines(rel_notes_ext)
    rmtree(tmpdir)


if __name__ == '__main__':
    main()
